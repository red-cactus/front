import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Components
import { IndexViewComponent } from './view/index-view/index-view.component';
import { UserDashboardViewComponent } from './view/user/user-dashboard-view/user-dashboard-view.component';
import { AdminDashboardViewComponent } from './view/admin/admin-dashboard-view/admin-dashboard-view.component';

// Component Parts
import { AdminSidebarAddonComponent } from './addons/admin/admin-sidebar-addon/admin-sidebar-addon.component';
import { AdminDashboardAddonComponent } from './addons/admin/admin-dashboard-addon/admin-dashboard-addon.component';
import { AdminUservalidationAddonComponent } from './addons/admin/admin-uservalidation-addon/admin-uservalidation-addon.component';
import { AdminUservalidationValidationcardComponent } from './addons/admin/admin-uservalidation-validationcard/admin-uservalidation-validationcard.component';
import { ProjectCardComponent } from './addons/user/project-card/project-card.component';


// Services
import { CommunicationsService } from './services/communications.service';
import { ImageConverterService } from './services/image-converter.service';


@NgModule({
  declarations: [
    AppComponent,
    IndexViewComponent,
    UserDashboardViewComponent,
    AdminDashboardViewComponent,
    AdminSidebarAddonComponent,
    AdminDashboardAddonComponent,
    AdminUservalidationAddonComponent,
    AdminUservalidationValidationcardComponent,
    ProjectCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    CommunicationsService,
    ImageConverterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
