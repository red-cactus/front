import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexViewComponent } from './view/index-view/index-view.component';
import { UserDashboardViewComponent } from './view/user/user-dashboard-view/user-dashboard-view.component';
import { AdminDashboardViewComponent } from './view/admin/admin-dashboard-view/admin-dashboard-view.component';
import { AdminDashboardAddonComponent } from './addons/admin/admin-dashboard-addon/admin-dashboard-addon.component';
import { AdminUservalidationAddonComponent } from './addons/admin/admin-uservalidation-addon/admin-uservalidation-addon.component';


const routes: Routes = [
  { path: '', component: IndexViewComponent},
  { path: 'user', component: UserDashboardViewComponent},
  { path: 'admin', component: AdminDashboardViewComponent, children: [
    { path: '', component: AdminDashboardAddonComponent, outlet: 'admin'},
    { path: 'dashboard', component: AdminDashboardAddonComponent, outlet: 'admin'},
    { path: 'uservalidation', component: AdminUservalidationAddonComponent, outlet: 'admin'}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
