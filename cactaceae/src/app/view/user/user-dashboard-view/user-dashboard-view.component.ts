import { Component, OnInit } from '@angular/core';
import { ImageConverterService } from '../../../services/image-converter.service';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Component({
  selector: 'app-user-dashboard-view',
  templateUrl: './user-dashboard-view.component.html',
  styleUrls: ['./user-dashboard-view.component.scss']
})
export class UserDashboardViewComponent implements OnInit {

  page_title = 'Dashboard';

  show_success_alert = false;
  success_msg = "";
  show_danger_alert = false;
  danger_msg = "";
  show_warning_alert = false;
  warning_msg = "";

  project_name = "Testing it works";
  project_description = "";
  project_file = null;

  constructor(private image_converter: ImageConverterService) { }

  ngOnInit() {
  }

  open_create_project_modal() {
    console.log('Flushing Modal');
    console.log(this.project_name);
    this.project_name = null;
    this.project_description = null;
    this.project_file = null;
  }

  create_project(e) {
    e.preventDefault()

    let p_name = e.target["input-project-name"].value;
    let p_description = e.target["input-project-description"].value;

    if (e.target["input-project-image"].files[0]) {

    } else {
      this.image_converter.convert_to_base64(e.target["input-project-image"].files[0], (p_image) => {

      });
    }
  }

  close_create_project_modal() {
    
  }

  alert_success(msg) {
    this.success_msg = msg;
    this.show_success_alert = true;
    setTimeout(function() {this.show_success_alert = false}, 3500);
  }

  alert_danger(msg) {
    this.danger_msg = msg;
    this.show_danger_alert = true;
    setTimeout(function() {this.show_danger_alert = false}, 3500);
  }

  alert_warning(msg) {
    this.warning_msg = msg;
    this.show_warning_alert = true;
    setTimeout(function() {this.show_warning_alert = false}, 3500);
  }

}
