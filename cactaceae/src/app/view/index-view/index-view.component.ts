import { Component, OnInit } from '@angular/core';
import { CommunicationsService } from '../../services/communications.service';
import { Router } from '@angular/router';
import { from } from 'rxjs';

@Component({
  selector: 'app-index-view',
  templateUrl: './index-view.component.html',
  styleUrls: ['./index-view.component.scss']
})
export class IndexViewComponent implements OnInit {

  error_connexion_display = false;
  alert_connexion_display = false;
  success_connexion_display = false;
  connexion_alert_msg = '';
  connexion_success_msg = '';


  constructor(private communications: CommunicationsService, private router: Router) { }

  ngOnInit() {
  }

  login(e) {
    e.preventDefault();
    const username = e.target.loginUsername.value;
    const password = e.target.loginPassword.value;

    this.communications.login_request(username, password).subscribe((data) => {
      if (data.success) {
        if (e.target.loginStayConnected.checked) {
          localStorage.setItem('auth', data.token);
        } else {
          sessionStorage.setItem('auth', data.token);
        }
        this.show_success_modal('Login Success');

        this.communications.verify_rights(data.token).subscribe((data) => {
          if (data.success) {
            if (data.is_admin) {
              this.router.navigate(['/admin']);
            } else {
              this.router.navigate(['/user']);
            }
          } else {
            this.show_error_modal();
          }
        });
      } else {
        if (data.msg == 'Unex Error') {
          this.show_error_modal();
        } else if (data.msg == 'bad cred') {
          this.show_alert_modal('Username or password invalid.');
        } else if (data.msg == 'User not validated') {
          this.show_alert_modal('Please wait, an admin is validating your registering');
        } else if (data.msg == 'User refused by admin') {
          this.show_alert_modal('Your registering was refused by an Admin. If this is an error, please contact the website administrator.');
        }
      }
    });
  }

  prepare_register(e) {
    e.preventDefault();
    // Verify Inputs
    const username = e.target.registerUsername.value;
    const email = e.target.registerEmail.value;
    const pass = e.target.registerPassword.value;
    const passVerif = e.target.registerPasswordVerify.value;
    const name = e.target.registerName.value;
    const surname = e.target.registerSurname.value;
    const phone = e.target.registerPhone.value;
    const post = e.target.registerPost.value;

    if (pass !== passVerif) {
      this.show_alert_modal('The passwords entered do not match.');
    } else if (!this.validate_password(pass)) {
      this.show_alert_modal('Password must contain at least 8 characters, one uppercase, one lowercase and one number.');
    } else if (!username || !email || !pass || !name || !surname || !phone || !post) {
      this.show_alert_modal('Please fill all the inputs');
    } else {
      this.register_user(username, email, pass, name, surname, phone, post, e);
    }
  }

  register_user(username, email, password, name, surname, phone, post, e) {
    this.communications.register_request(username, email, password, name, surname, phone, post).subscribe((data) => {
      if (data.success) {
        this.show_success_modal('User registered with success, please wait for an administrator to validate your registering.');
        setTimeout(function() {
          window.location.reload();
        }.bind(this), 4000)
      } else {
        if (data.msg == 'Unex Error') {
          this.show_error_modal();
        } else if (data.msg == 'email_taken' || data.msg == 'user_taken') {
          this.show_alert_modal('Unfortunately the username or email is already used...');
        }
      }
    })
  }

  validate_password(pass) {
    // Password of minimum 8 char, 1 upper, 1 lower, 1 number
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(pass);
  }

  show_error_modal() {
    this.error_connexion_display = true;
    setTimeout(function() {
      this.error_connexion_display = false;
    }.bind(this), 4000)
  }

  show_alert_modal(msg) {
    this.connexion_alert_msg = msg;
    this.alert_connexion_display = true;
    setTimeout(function() {
      this.connexion_alert_msg = '';
      this.alert_connexion_display = false;
    }.bind(this), 4000)
  }

  show_success_modal(msg) {
    this.success_connexion_display = true;
    this.connexion_success_msg = msg;
    setTimeout(function() {
      this.success_connexion_display = false;
      this.connexion_success_msg = '';
    }.bind(this), 3500)
  }

}
