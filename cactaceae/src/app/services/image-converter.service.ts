import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageConverterService {

  constructor() { }

  convert_to_base64(input_file, callback) {
    var reader = new FileReader();
    reader.onloadend = function() {
      callback(reader.result);
    }
    reader.readAsDataURL(input_file);
  }

  convert_from_base64(b64_string) {

  }
}
