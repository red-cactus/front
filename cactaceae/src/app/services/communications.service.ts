import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

interface basicPostResponse {
  success: boolean,
  msg: string
}

interface RegisterPostResponse {
  success: boolean,
  msg: string
}

interface LoginPostResponse {
  success: boolean,
  msg: string,
  token: string
}

interface VerifyRightsPostResponse {
  success: boolean,
  is_admin: boolean,
  msg: string
}

interface UserToValidateResponse {
  success: boolean,
  users: any
}

@Injectable({
  providedIn: 'root'
})

export class CommunicationsService {
  private server_url = "http://localhost:7535";
  
  constructor(private http: HttpClient) {}

  get_auth_token() {
    if (localStorage.getItem('auth')) {
      return localStorage.getItem('auth');
    } else {
      return sessionStorage.getItem('auth');
    }
  }


  register_request(username, email, password, name, surname, phone, post): Observable<RegisterPostResponse> {
    return this.http.post<RegisterPostResponse>((this.server_url + '/users/new'), {'username': username, 'email': email, 'password': password, 'name': name, 'surname': surname, 'phone': phone, 'post': post});
  }

  login_request(username, password): Observable<LoginPostResponse> {
    return this.http.post<LoginPostResponse>((this.server_url + '/users/login'), {'username': username, 'password': password});
  }

  verify_rights(token): Observable<VerifyRightsPostResponse> {
    httpOptions.headers.set('Authorization', this.get_auth_token());
    return this.http.post<VerifyRightsPostResponse>((this.server_url + '/admin/is-admin'), {'token': this.get_auth_token()}, httpOptions)
  }

  get_user_to_validate_request(token): Observable<UserToValidateResponse> {
    httpOptions.headers.set('Authorization', this.get_auth_token());
    return this.http.post<UserToValidateResponse>((this.server_url + '/admin/get-users-to-validate'), {'token': this.get_auth_token()}, httpOptions)
  }

  validate_user_request(token, validation, user_id): Observable<basicPostResponse> {
    httpOptions.headers.set('Authorization', this.get_auth_token());
    return this.http.post<basicPostResponse>((this.server_url + '/admin/validate-user'), {'token': this.get_auth_token(), 'user_id': user_id, 'validated': validation}, httpOptions)
  }
}


