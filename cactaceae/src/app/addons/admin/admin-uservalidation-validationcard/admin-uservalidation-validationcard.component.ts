import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommunicationsService } from '../../../services/communications.service';

@Component({
  selector: 'app-admin-uservalidation-validationcard',
  template: `
    <div class="validation-card">
      <button class="validate-card-drop-btn" [attr.data-target]="'#card-id-' + user.username" data-toggle="collapse">{{user.username}}</button>
      <div class="collapse collapse-user-validation" [attr.id]="'card-id-' + user.username">
        <div class="validate-row">
          <div class="validate-cell">
            <p>Name: <span>{{user.name}}</span></p>
          </div>
          <div class="validate-cell">
            <p>Surname: <span>{{user.surname}}</span></p>
          </div>
        </div>
        <div class="validate-row">
          <div class="validate-cell">
            <p>Phone: <span>{{user.phone}}</span></p>
          </div>
          <div class="validate-cell">
            <p>Post: <span>{{user.post}}</span></p>
          </div>
        </div>
        <div class="validate-row">
          <div class="validate-cell">
            <p>E-Mail: <span>{{user.email}}</span></p>
          </div>
        </div>

        <div class="validate-user-btns">
          <button class="btn refuse-user-btn" (click)="validate_user(false)">Refuse</button>
          <button class="btn validate-user-btn" (click)="validate_user(true)">Validate</button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./admin-uservalidation-validationcard.component.scss']
})

export class AdminUservalidationValidationcardComponent implements OnInit {

  @Input() user: any;
  @Output() ask_reload = new EventEmitter<boolean>();
  // @Input() message: any;


  constructor(private communications: CommunicationsService) { }

  ngOnInit() {
  }

  validate_user(validation) {
    this.communications.validate_user_request(localStorage.getItem('auth'), validation, this.user.id).subscribe((data) => {
      if (data.success) {
        this.ask_reload.emit(true);
      } else {
        alert('Error...')
      }
    })
  }

}
