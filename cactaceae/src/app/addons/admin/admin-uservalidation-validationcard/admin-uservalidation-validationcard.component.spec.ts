import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUservalidationValidationcardComponent } from './admin-uservalidation-validationcard.component';

describe('AdminUservalidationValidationcardComponent', () => {
  let component: AdminUservalidationValidationcardComponent;
  let fixture: ComponentFixture<AdminUservalidationValidationcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUservalidationValidationcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUservalidationValidationcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
