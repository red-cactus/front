import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSidebarAddonComponent } from './admin-sidebar-addon.component';

describe('AdminSidebarAddonComponent', () => {
  let component: AdminSidebarAddonComponent;
  let fixture: ComponentFixture<AdminSidebarAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSidebarAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSidebarAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
