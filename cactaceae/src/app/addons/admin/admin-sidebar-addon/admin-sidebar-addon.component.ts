import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-sidebar-addon',
  templateUrl: './admin-sidebar-addon.component.html',
  styleUrls: ['./admin-sidebar-addon.component.scss']
})
export class AdminSidebarAddonComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  show_validate_user_page() {
    this.router.navigate([{outlets: {primary: '' ,sidebar: 'user-validation'}}]);
  }

  show_projects_page() {

  }

}
