import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUservalidationAddonComponent } from './admin-uservalidation-addon.component';

describe('AdminUservalidationAddonComponent', () => {
  let component: AdminUservalidationAddonComponent;
  let fixture: ComponentFixture<AdminUservalidationAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUservalidationAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUservalidationAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
