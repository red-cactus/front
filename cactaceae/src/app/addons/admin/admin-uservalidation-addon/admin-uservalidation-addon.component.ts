import { Component, OnInit } from '@angular/core';
import { CommunicationsService } from '../../../services/communications.service';

@Component({
  selector: 'app-admin-uservalidation-addon',
  templateUrl: './admin-uservalidation-addon.component.html',
  styleUrls: ['./admin-uservalidation-addon.component.scss']
})


export class AdminUservalidationAddonComponent implements OnInit {

  user_to_validate_list = []

  message = 'test message';

  constructor(private communications: CommunicationsService) { }

  ngOnInit() {
    this.get_user_to_validate()
  }

  get_user_to_validate() {
    this.communications.get_user_to_validate_request(sessionStorage.getItem('auth')).subscribe((data) => {
      if (data.success) {
        this.user_to_validate_list = data.users;
      } else {
        alert('ERROR');
      }
    })
  }

  reload_user_list(e) {
    this.get_user_to_validate();
  }

}
