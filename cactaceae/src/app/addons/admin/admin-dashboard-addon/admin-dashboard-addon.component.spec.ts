import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashboardAddonComponent } from './admin-dashboard-addon.component';

describe('AdminDashboardAddonComponent', () => {
  let component: AdminDashboardAddonComponent;
  let fixture: ComponentFixture<AdminDashboardAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashboardAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
